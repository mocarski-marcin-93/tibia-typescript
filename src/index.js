"use strict";
document.getElementsByTagName('BODY')[0].appendChild(document.createTextNode('Hello world!'));
var map_1 = require('./Map/map');
var hero_1 = require('./Hero/hero');
var screen_1 = require('./Screen/screen');
var $ = require('../node_modules/jquery');
var map = new map_1.Map(30, 30);
var hero = new hero_1.Hero(10, 10);
var screen = new screen_1.Screen(13, 13, hero.posX - 6, hero.posY - 6);
var move = '';
map.fill();
screen.setScreen(map.getMapPiece(screen.startX, screen.startY, screen.sizeX, screen.sizeY));
screen.print();
activate(screen);
hero.print();
function Game() {
    screen.clean();
    screen.setScreen(map.getMapPiece(screen.startX, screen.startY, screen.sizeX, screen.sizeY));
    screen.print();
    screen.move(move);
    hero.move(move);
    move = '';
    activate(screen);
    setTimeout(function () {
        Game();
    }, 500);
}
Game();
function activate(screen) {
    $(window).one('keydown', function (e) {
        if (e.which === 38) {
            move = 'up';
        }
        if (e.which === 37) {
            move = 'left';
        }
        if (e.which === 39) {
            move = 'right';
        }
        if (e.which === 40) {
            move = 'down';
        }
    });
}
