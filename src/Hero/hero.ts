import { IMinion } from '../Minion/minion.interface';
import { IHero } from './hero.interface';

export class Hero implements IHero {

    image: string;

    posX: number;
    posY: number;

    maxHealth: number;
    currentHealth: number;
    experience: number;
    level: number;
    damage: number;

    isAttack: boolean;
    attackTarget: IMinion;

    constructor(posX: number, posY: number) {
      this.posX = posX;
      this.posY = posY;
    }
    
    print(): void {
        var newHero = document.createElement('DIV');
        newHero.setAttribute('class', 'hero');
        newHero.setAttribute('id', 'hero');
        document.getElementsByTagName('body')[0].appendChild(newHero);
    }

    move(where: string): void {
        let _this = this;
        switch (where) {
            case 'up': {
                _this.posY--;
                break;
            }
            case 'down': {
                _this.posY++;
                break;
            }
            case 'left': {
                _this.posX--;
                break;
            }
            case 'right': {
                _this.posX++;
                break;
            }
        }
        console.log('Hero.move() now XY: ' + this.posX + this.posY);
    }

    attack(): void {

    }
}
