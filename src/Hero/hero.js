"use strict";
var Hero = (function () {
    function Hero(posX, posY) {
        this.posX = posX;
        this.posY = posY;
    }
    Hero.prototype.print = function () {
        var newHero = document.createElement('DIV');
        newHero.setAttribute('class', 'hero');
        newHero.setAttribute('id', 'hero');
        document.getElementsByTagName('body')[0].appendChild(newHero);
    };
    Hero.prototype.move = function (where) {
        var _this = this;
        switch (where) {
            case 'up': {
                _this.posY--;
                break;
            }
            case 'down': {
                _this.posY++;
                break;
            }
            case 'left': {
                _this.posX--;
                break;
            }
            case 'right': {
                _this.posX++;
                break;
            }
        }
        console.log('Hero.move() now XY: ' + this.posX + this.posY);
    };
    Hero.prototype.attack = function () {
    };
    return Hero;
}());
exports.Hero = Hero;
