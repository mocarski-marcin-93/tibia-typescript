import { IMinion } from '../Minion/minion.interface';

export interface IHero {

    image: string;

    posX: number;
    posY: number;

    maxHealth: number;
    currentHealth: number;
    experience: number;
    level: number;
    damage: number;

    isAttack: boolean;
    attackTarget: IMinion;

    print(): void;
    move(where: string): void;
    attack(): void;
}
