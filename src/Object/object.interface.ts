export interface IObject {
  name: string;   // tree, stone etc.
  image: string;  // depends on name
  next: IObject;  // if there are more than one object on field, we make unidirectional list
}
