import { IObject } from './object.interface';

export class Object implements IObject {

  name: string;
  image: string;
  next: IObject | null;

  constructor(name: string) {
    this.name = name;
  }

}
