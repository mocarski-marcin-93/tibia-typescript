"use strict";
var Ground = (function () {
    function Ground(type) {
        this.type = type;
        switch (this.type) {
            case 'grass':
                this.image = 'img/ground-grass.png';
                break;
        }
    }
    return Ground;
}());
exports.Ground = Ground;
