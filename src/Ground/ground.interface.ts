export interface IGround {
  type: string;     // water, grass etc.
  image: string;    // depends on type
}
