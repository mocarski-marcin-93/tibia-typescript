import { IGround } from './ground.interface';

export class Ground implements IGround {

    type: string;
    image: string;

    constructor(type: string) {
        this.type = type;
        switch (this.type) {
            case 'grass':
                this.image = 'img/ground-grass.png';
                break;
        }
    }
}
