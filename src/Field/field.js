"use strict";
var ground_1 = require('../Ground/ground');
var object_1 = require('../Object/object');
var minion_1 = require('../Minion/minion');
var Field = (function () {
    function Field() {
        this.ground = new ground_1.Ground('');
        this.object = new object_1.Object('');
        this.minion = new minion_1.Minion();
    }
    Field.prototype.canIEnter = function () {
        return true;
    };
    return Field;
}());
exports.Field = Field;
