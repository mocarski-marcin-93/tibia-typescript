import { IField } from './field.interface';
import { IGround } from '../Ground/ground.interface';
import { IObject } from '../Object/object.interface';
import { IMinion } from '../Minion/minion.interface';
import { Ground } from '../Ground/ground';
import { Object } from '../Object/object';
import { Minion } from '../Minion/minion';

export class Field implements IField {

    ground: IGround;
    object: IObject;
    minion: IMinion;

    constructor() {
        this.ground = new Ground('');
        this.object = new Object('');
        this.minion = new Minion();
    }
    canIEnter(): boolean {
        return true;
    }

}
