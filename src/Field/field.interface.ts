import { IGround } from '../Ground/ground.interface';
import { IObject } from '../Object/object.interface';
import { IMinion } from '../Minion/minion.interface';

export interface IField {

  ground: IGround;
  object: IObject;
  minion: IMinion;

  canIEnter(): boolean;
}
