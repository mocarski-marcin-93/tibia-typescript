import { IMinion } from './minion.interface';

export class Minion implements IMinion {

  id: number;
  name: string;
  image: string;

  posX: number;
  posY: number;

  maxHealth: number;
  currentHealth: number;
  damage: number;
  experience: number;

  isMove: boolean;
  moveDirection: string;
  isFollow: boolean;
  isAttack: boolean;

  move(): void {
    console.log('minion move to: ' + this.moveDirection);
  }

  follow(): void {
    console.log('minion follows');
  }

  attack(): void {
    console.log('minions attacks');
  }

  activate(): void {
    console.log('minion activated');
  }

  distanceFrom(): number {
    return -1;
  }
}
