"use strict";
var Minion = (function () {
    function Minion() {
    }
    Minion.prototype.move = function () {
        console.log('minion move to: ' + this.moveDirection);
    };
    Minion.prototype.follow = function () {
        console.log('minion follows');
    };
    Minion.prototype.attack = function () {
        console.log('minions attacks');
    };
    Minion.prototype.activate = function () {
        console.log('minion activated');
    };
    Minion.prototype.distanceFrom = function () {
        return -1;
    };
    return Minion;
}());
exports.Minion = Minion;
