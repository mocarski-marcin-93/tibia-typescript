export interface IMinion {

  id: number;
  name: string;
  image: string;

  posX: number;
  posY: number;

  maxHealth: number;
  currentHealth: number;

  damage: number;
  experience: number;

  isMove: boolean;
  moveDirection: string;
  isFollow: boolean;
  isAttack: boolean;

  move(): void;
  follow(): void;
  attack(): void;
  activate(): void;     // listeners

  distanceFrom(): number;
}
