document.getElementsByTagName('BODY')[0].appendChild(document.createTextNode('Hello world!'));

import { Field } from './Field/field';
import { Ground } from './Ground/ground';
import { Map } from './Map/map';
import { Hero } from './Hero/hero';
import { Minion } from './Minion/minion';
import { Object } from './Object/object';
import { Screen } from './Screen/screen';

var $ = require('../node_modules/jquery');

// prepare game
var map = new Map(30, 30);
var hero = new Hero(10, 10);
var screen = new Screen(13, 13, hero.posX - 6, hero.posY - 6);
var move: string = '';

map.fill();
screen.setScreen(map.getMapPiece(screen.startX, screen.startY, screen.sizeX, screen.sizeY));
screen.print();
activate(screen);
hero.print();

function Game() {
    // 1. Cleaning last screen
    screen.clean();
    // 2. Getting new screen
    screen.setScreen(map.getMapPiece(screen.startX, screen.startY, screen.sizeX, screen.sizeY));
    // 3. Printing new screen
    screen.print();
    // 4. Making move if user wants to
    screen.move(move);
    hero.move(move);
    // 5. Reseting move setting
    move = '';
    // 6. Activate listeners
    activate(screen);
    // 7. Loop
    setTimeout(function() {
        Game();
    }, 500);
}
Game();

function activate(screen) {
    // 1. Move listeners
    $(window).one('keydown', function(e) {
        if (e.which === 38) {
            move = 'up';
        }
        if (e.which === 37) {
            move = 'left';
        }
        if (e.which === 39) {
            move = 'right';
        }
        if (e.which === 40) {
            move = 'down';
        }
    });
    // 2. Attack listeners
}
