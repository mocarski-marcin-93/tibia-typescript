import { IField } from '../Field/field.interface';

export interface IScreen {

    sizeX: number;
    sizeY: number;

    screen: IField[][];

    setScreen(mapPiece: IField[][]): void;    // we will get that thanks to Map.getMapPiece
    activateCreatures(): void;

    clean(): void;
    print(): void;
    move(direction: string);
}
