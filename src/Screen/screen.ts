import { IScreen } from './screen.interface';
import { IField } from '../Field/field.interface';

var $ = require('../../node_modules/jquery');

export class Screen implements IScreen {

    sizeX: number;
    sizeY: number;

    startX: number;
    startY: number;

    screen: IField[][];

    constructor(sizeX: number, sizeY: number, startX: number, startY: number) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.startX = startX;
        this.startY = startY;

        this.screen = new Array(this.sizeX);
        for (let i = 0; i < this.sizeX; i++) {
            this.screen[i] = new Array(this.sizeY);
        }
    }

    setScreen(mapPiece: IField[][]): void {
        for (let i = 0; i < this.sizeX; i++) {
            for (let j = 0; j < this.sizeY; j++) {
                this.screen[i][j] = mapPiece[i][j];
            }
        }
    }

    activateCreatures(): void {
        console.log('activating creatures...');
    }

    clean(): void {
        document.getElementById('screenWrapper').innerHTML = '';
    }

    print(): void {
        var newScreen = document.createElement('DIV');
        newScreen.setAttribute('id', 'screen');
        newScreen.setAttribute('class', 'screen');
        for (let i = 0; i < this.sizeX; i++) {
            for (let j = 0; j < this.sizeY; j++) {
                var newField = document.createElement('DIV');
                newField.setAttribute('class', 'field');
                newField.style.backgroundImage = "url('" + this.screen[i][j].ground.image + "')";
                newField.style.left = 32 * i + 'px';
                newField.style.top = 32 * j + 'px';
                newScreen.appendChild(newField);
            }
        }
        document.getElementById('screenWrapper').appendChild(newScreen);
    }

    move(direction: string): void {
        var screen = document.getElementById('screen');
        switch (direction) {
            case '': {
                return;
            }
            case 'up': {
                if (this.startY < 1) return;
                $('#screen').animate({
                    top: '+32px'
                }, 485, function() {
                    console.log('Screen.move() - moved up');
                });
                this.startY--;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'down': {
                $('#screen').animate({
                    top: '-32px'
                }, 485, function() {
                    console.log('Screen.move() - moved down');
                });
                this.startY++;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'left': {
                if (this.startX < 1) return;
                $('#screen').animate({
                    left: '+32px'
                }, 485, function() {
                    console.log('Screen.move() - moved left');
                });
                this.startX--;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'right': {
                $('#screen').animate({
                    left: '-32px'
                }, 485, function() {
                    console.log('Screen.move() - moved right');
                });
                this.startX++;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
        }
    }
}
