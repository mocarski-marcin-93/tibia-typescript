"use strict";
var $ = require('../../node_modules/jquery');
var Screen = (function () {
    function Screen(sizeX, sizeY, startX, startY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.startX = startX;
        this.startY = startY;
        this.screen = new Array(this.sizeX);
        for (var i = 0; i < this.sizeX; i++) {
            this.screen[i] = new Array(this.sizeY);
        }
    }
    Screen.prototype.setScreen = function (mapPiece) {
        for (var i = 0; i < this.sizeX; i++) {
            for (var j = 0; j < this.sizeY; j++) {
                this.screen[i][j] = mapPiece[i][j];
            }
        }
    };
    Screen.prototype.activateCreatures = function () {
        console.log('activating creatures...');
    };
    Screen.prototype.clean = function () {
        document.getElementById('screenWrapper').innerHTML = '';
    };
    Screen.prototype.print = function () {
        var newScreen = document.createElement('DIV');
        newScreen.setAttribute('id', 'screen');
        newScreen.setAttribute('class', 'screen');
        for (var i = 0; i < this.sizeX; i++) {
            for (var j = 0; j < this.sizeY; j++) {
                var newField = document.createElement('DIV');
                newField.setAttribute('class', 'field');
                newField.style.backgroundImage = "url('" + this.screen[i][j].ground.image + "')";
                newField.style.left = 32 * i + 'px';
                newField.style.top = 32 * j + 'px';
                newScreen.appendChild(newField);
            }
        }
        document.getElementById('screenWrapper').appendChild(newScreen);
    };
    Screen.prototype.move = function (direction) {
        var screen = document.getElementById('screen');
        switch (direction) {
            case '': {
                return;
            }
            case 'up': {
                if (this.startY < 1)
                    return;
                $('#screen').animate({
                    top: '+32px'
                }, 485, function () {
                    console.log('Screen.move() - moved up');
                });
                this.startY--;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'down': {
                $('#screen').animate({
                    top: '-32px'
                }, 485, function () {
                    console.log('Screen.move() - moved down');
                });
                this.startY++;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'left': {
                if (this.startX < 1)
                    return;
                $('#screen').animate({
                    left: '+32px'
                }, 485, function () {
                    console.log('Screen.move() - moved left');
                });
                this.startX--;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
            case 'right': {
                $('#screen').animate({
                    left: '-32px'
                }, 485, function () {
                    console.log('Screen.move() - moved right');
                });
                this.startX++;
                console.log('Screen.move() - now XY: ' + this.startX + this.startY);
                break;
            }
        }
    };
    return Screen;
}());
exports.Screen = Screen;
