"use strict";
var field_1 = require('../Field/field');
var ground_1 = require('../Ground/ground');
var Map = (function () {
    function Map(sizeX, sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.map = new Array(sizeX);
        for (var i = 0; i < this.sizeX; i++) {
            this.map[i] = new Array(this.sizeY);
            for (var j = 0; j < this.sizeY; j++) {
                this.map[i][j] = new field_1.Field();
            }
        }
    }
    Map.prototype.fill = function () {
        for (var i = 0; i < this.sizeX; i++) {
            for (var j = 0; j < this.sizeY; j++) {
                this.map[i][j].ground = new ground_1.Ground('grass');
            }
        }
    };
    Map.prototype.getMapPiece = function (fromX, fromY, sizeX, sizeY) {
        var mapPiece;
        mapPiece = new Array(sizeX);
        for (var k = 0; k < sizeX; k++) {
            mapPiece[k] = new Array(sizeY);
        }
        for (var i = 0; i < sizeX; i++) {
            for (var j = 0; j < sizeY; j++) {
                mapPiece[i][j] = this.map[fromX + i][fromY + j];
            }
        }
        return mapPiece;
    };
    return Map;
}());
exports.Map = Map;
