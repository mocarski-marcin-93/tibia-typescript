import { IMap } from './map.interface';
import { IField } from '../Field/field.interface';
import { Field } from '../Field/field';
import { Ground } from '../Ground/ground';

export class Map implements IMap {

    sizeX: number;
    sizeY: number;

    map: IField[][];

    constructor(sizeX, sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        this.map = new Array(sizeX);
        for (let i = 0; i < this.sizeX; i++) {
            this.map[i] = new Array(this.sizeY)
            for(let j = 0; j < this.sizeY; j++) {
              this.map[i][j] = new Field();
            }
        }
    }

    fill() {
        for(let i = 0; i < this.sizeX; i++) {
          for(let j = 0; j < this.sizeY; j++) {
            this.map[i][j].ground = new Ground('grass');
          }
        }
    }

    getMapPiece(fromX: number, fromY: number, sizeX: number, sizeY: number): IField[][] {

        let mapPiece: IField[][];
        mapPiece = new Array(sizeX);
        for (let k = 0; k < sizeX; k++) {
            mapPiece[k] = new Array(sizeY);
        }

        for (let i = 0; i < sizeX; i++) {
            for (let j = 0; j < sizeY; j++) {
                mapPiece[i][j] = this.map[fromX + i][fromY + j];
            }
        }

        return mapPiece;
    }

}
