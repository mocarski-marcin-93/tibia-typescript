import { IField } from '../Field/field.interface';

export interface IMap {

  sizeX: number;
  sizeY: number;

  map: IField[][];

  fill(): void;

  getMapPiece(fromX: number, fromY: number, sizeX: number, sizeY: number): IField[][];

}
